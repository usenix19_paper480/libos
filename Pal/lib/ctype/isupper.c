#include "api.h"

/* ANONYMOUS: from muslibc */

int
isupper(int c)
{
    return ((((unsigned)c) - 'A') < 26);
}
