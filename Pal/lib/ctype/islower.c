#include "api.h"

/* ANONYMOUS: from muslibc */

int
islower(int c)
{
    return ((((unsigned)c) - 'a') < 26);
}
